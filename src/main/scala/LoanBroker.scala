import akka.actor.ActorRef

case class QuoteBestLoanRate(taxId: String, amount: Integer, termInMonths: Integer)

case class BestLoanRateQuoted(bankId: String,
                              loanRateQuoteId: String,
                              taxId: String,
                              amount: Integer,
                              termInMonths: Integer,
                              creditScore: Integer,
                              interestRate: Double)

case class BestLoanRateDenied (loanRateQuoteId: String,
                               taxId: String,
                               amount: Integer,
                               termInMonths: Integer,
                               creditScore: Integer)

class LoanBroker(creditBureau:ActorRef, banks: Seq[ActorRef]) extends ProcessManager {

  override def receive: Receive = {
    case message: BankLoanRateQuoted =>
      log.info(s"$message")

      processOf(message.loanQuoteReferenceId) !
        RecordLoanRateQuote(message.bankId,
          message.bankLoanRateQuoteId,
          message.interestRate
        )

    case message: CreditChecked =>
      log.info(s"$message")

      processOf(message.creditProcessingRefernceId) !
      EstablishCreditScoreForLoanRateQuote(
        message.creditProcessingRefernceId,
        message.taxId,
        message.score
      )

    case message: CreditScoreForLoanRateQuoteDenied =>
      println(s"$message")

      processOf(message.loanRateQuoteId) ! TerminateLoanRateQuote()

      val denied = BestLoanRateDenied(
        message.loanRateQuoteId,
        message.taxId,
        message.amount,
        message.termInMonths,
        message.score
      )
      log.info(s"Would be sent to original requester $denied")


    case message: CreditScoreForLoanRateQuoteEstablished =>
      log.info(s"$message")

      banks map { bank=>
        bank ! QuoteLoanRate(
          message.loanRateQuoteId,
          message.taxId,
          message.score,
          message.amount,
          message.termInMonths
        )
      }


    case message: LoanRateBestQuoteFilled =>
      log.info(s"$message")

      stopProcess(message.loanRateQuoteId)

      val best = BestLoanRateQuoted(
        message.bestBankLoanRateQuote.bankId,
        message.loanRateQuoteId,
        message.taxId,
        message.amount,
        message.termInMonths,
        message.creditScore,
        message.bestBankLoanRateQuote.interestRate
      )

      log.info(s"Would be sent to the original requester ${best}" )

    case message: LoanRateQuoteStarted =>
      log.info(s"$message")

      creditBureau ! CheckCredit(message.loanRateQuoteId,
        message.taxId)


    case message: LoanRateQuoteTerminated =>
      log.info(s"$message")

      stopProcess(message.loanRateQuoteId)

    case message:ProcessStarted =>
      log.info(s"$message")

      message.process ! StartLoanRateQuote(banks.size)

    case message: ProcessStopped =>
      log.info(s"$message")

      context.stop(message.process)

    case message: QuoteBestLoanRate =>
      val loanRateQuoteId  = LoanRateQuote.id

      log.info(s"$message for $loanRateQuoteId")

      val loanRateQuote = LoanRateQuote(
        loanRateQuoteId,
        message.taxId,
        message.amount,
        message.termInMonths,
        self
      )

      startProcess(loanRateQuoteId,loanRateQuote)
  }
}
