import akka.actor.{Actor, ActorRef}
import akka.event.{Logging, LoggingAdapter}

case class ProcessStarted(processId: String, process: ActorRef)
case class ProcessStopped(processId: String, process: ActorRef)

abstract class ProcessManager extends Actor{

  private var processes = Map[String, ActorRef]()

  var log: LoggingAdapter = Logging.getLogger(context.system, self)

  def processOf(processId : String):ActorRef = {
    if(processes.contains(processId)){
      processes(processId)
    }
    else {
      null ///??? seriously
    }
  }

  def startProcess(processId: String, process: ActorRef) = {
    if(!processes.contains(processId)){
      processes = processes+ (processId -> process)
    }
  }

  def stopProcess(processId: String) = {
    if(processes.contains(processId)){
      val currProcess = processes(processId)
      processes = processes - processId

      self ! ProcessStopped(processId, currProcess)
    }
  }
}
