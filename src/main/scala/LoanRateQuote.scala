import akka.actor.ActorSystem.Settings
import akka.actor.{Actor, ActorPath, ActorRef, ActorRefProvider, ActorSystem, ActorSystemImpl, Extension, ExtensionId, InternalActorRef, Props, Scheduler, Terminated}
import akka.dispatch.{Dispatchers, Mailboxes}
import akka.event.{EventStream, LoggingAdapter}
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Random


case class StartLoanRateQuote(expectedLoanRateQuotes: Integer)
case class LoanRateQuoteStarted(loanRateQuoteId: String,
                                taxId: String)

case class TerminateLoanRateQuote()

case class LoanRateQuoteTerminated(loanRateQuoteId:String,
                                   taxId: String)

case class EstablishCreditScoreForLoanRateQuote(loanRateQuoteId: String,
                                               taxId: String,
                                               score:Integer)

case class CreditScoreForLoanRateQuoteEstablished(loanRateQuoteId: String,
                                                 taxId: String,
                                                 score: Integer,
                                                 amount: Integer,
                                                 termInMonths: Integer)

case class CreditScoreForLoanRateQuoteDenied(loanRateQuoteId: String,
                                             taxId: String,
                                             amount: Integer,
                                             termInMonths: Integer,
                                             score: Integer)
case class RecordLoanRateQuote(bankId: String,
                               bankLoanRateQuoteId: String,
                               interestRate: Double)

case class LoanRateQuoteRecorded(loanRateQuoteId: String,
                                 taxId: String,
                                 bankLoanRateQuoted: BankLoanRateQuote)

case class LoanRateBestQuoteFilled(loanRateQuoteId: String,
                                   taxId: String,
                                   amount: Integer,
                                   termInMonths: Integer,
                                   creditScore: Integer,
                                   bestBankLoanRateQuote: BankLoanRateQuote)

case class BankLoanRateQuote(bankId: String,
                             bankLoanRateQuoteId: String,
                             interestRate: Double)

object LoanRateQuote{
  val randomLoanRateQuoteId = new Random()

  val system = ActorSystem("loan-rate-as")
  def apply(loanRateQuoteId: String,
            taxId: String,
            amount: Integer,
            termInMonths: Integer,
            loanBroker: ActorRef): ActorRef ={
    val loanRateQuote = system.actorOf(Props(classOf[LoanRateQuote],
      loanRateQuoteId,
      taxId,
      amount,
      termInMonths,
      loanBroker),"loanRateQuote-"+loanRateQuoteId)

    loanRateQuote
  }

  def id() =
    randomLoanRateQuoteId.nextInt(1000).toString
}
class LoanRateQuote(loanRateQuoteId: String,
                    taxId: String,
                    amount: Integer,
                    termInMonths: Integer,
                    loanBroker: ActorRef) extends Actor {

  val bankLoanRateQuotes = Vector[BankLoanRateQuoted]()
  var creditRatingScore: Int = _
  var expectedLoanRateQuotes: Int = _

  private def bestBankLoanRateQuote()= {
    var best = bankLoanRateQuotes(0)

    bankLoanRateQuotes map { bankLoanRateQuote =>
      if(best.interestRate > bankLoanRateQuote.interestRate)
        best = bankLoanRateQuote
    }
    best
  }

  private def quotableCreditScore(score: Integer):Boolean =
    score > 399


  def receive = {
    case message: StartLoanRateQuote =>
      expectedLoanRateQuotes= message.expectedLoanRateQuotes

  }
}
