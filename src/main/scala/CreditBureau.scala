import akka.actor.{Actor, ActorRef, ActorSystem, Props}

import scala.util.Random

case class CheckCredit(creditProcessingReferenceId: String,
                       taxId: String)

case class CreditChecked(creditProcessingRefernceId: String,
                        taxId: String,
                        score: Integer)

object CreditBureau {
  val system = ActorSystem("lCreditBureau-as")

  def apply( ): ActorRef ={
    system.actorOf(Props(classOf[CreditBureau]),"creditbureau-")
  }
}

class CreditBureau extends Actor{

  val creditRanges = Vector(300, 400, 500, 600, 700)
  val randomCreditRangeGenerator = new Random()
  val randomCreditScoreGenerator = new Random()

  def receive = {
    case message: CheckCredit =>
      val range = creditRanges(randomCreditRangeGenerator.nextInt(3))
      val score = range+ randomCreditRangeGenerator.nextInt(20)

      sender ! CreditChecked(message.creditProcessingReferenceId,
        message.taxId,
        score)
  }

}
