import akka.actor.Actor
import akka.actor.Actor.Receive

import scala.util.Random

case class BankLoanRateQuoted(bankId: String,
                              bankLoanRateQuoteId: String,
                              loanQuoteReferenceId: String,
                              taxId: String,
                              interestRate: Double)

case class QuoteLoanRate(loadQuoteReferenceId: String,
                         taxId: String,
                         creditScore: Integer,
                         amount: Integer,
                         termInMonths: Integer)



class Bank( bankId: String,
            primeRate: Double,
            ratePremium: Double) extends Actor{

  val randomDiscount = new Random()

  val randomQuoteId = new Random()

  private def calculateInterestRate(amount: Double,
                                    months: Double,
                                    creditScore: Double) ={
    val creditScoreDiscount = creditScore /100.0 / 10.0 - (randomDiscount.nextInt(5) *0.05)

    primeRate + ratePremium + ((months /12.0) / 10.0) - creditScoreDiscount
  }

  override def receive: Receive = {
    case message: QuoteLoanRate =>
      val interestRate = calculateInterestRate(message.amount.toDouble,
        message.termInMonths.toDouble,
        message.creditScore.toDouble)

      sender ! BankLoanRateQuoted(bankId,randomQuoteId.nextInt(1000).toString,
        message.loadQuoteReferenceId, message.taxId, interestRate)
  }
}
